import React, { useState } from 'react'

import imgs from "../../assets/images/card2img/01.jpg"

const Cardsec = ({ demoval }) => {
    const myStyle = {
        width: "18rem"
    };
    return (
        <>
            {demoval.map((item) => {
                const { id, date, cotegary, batch, profName } = item;
                return (
                    <div className="col">
                    <div className="card" >
                        <img src={imgs} className="card-img-top" alt="..." />
                        <div className="card-body">
                        <h6 className="card-title text-dark mb-1 d-inline-block text-truncate w-100" data-bs-toggle="tooltip" title="Essentials of HRM ">Essentials of HRM</h6>
                        <div className="text-nowrap text-black mb-1">	
												 							
												<span className="d-block text-truncate w-100">
												Prof. Hiteshwari  Jadeja</span>
												 
												{/* <!-- <i className="fa-solid fa-circle-user fa-xl"></i> --> */}
												</div>
                                                <p className="d-inline-block text-truncate w-100 " data-bs-toggle="tooltip" title="Essentials of HRM Session 3 ">Essentials of HRM Session 3 </p>
                                                <p className="card-text  text-muted">Tuesday, 07-Feb-2023</p>
                                                <p className="card-text text-muted WeekDayBatch-text"> WeekDay Batch</p>
                            <h5 className="card-title">{id}</h5>
                        </div>
                    </div>
                    </div>
                )
            })
            }


        </>
    )
}

export default Cardsec