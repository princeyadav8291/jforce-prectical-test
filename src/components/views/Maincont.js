import React, { useState } from 'react';
import Cardinf from './../../assets/js/demo.js';
import Card from './Card';
import Cardsec from './Cardsec';
import Cardthree from './Cardthree.js';


const Maincont = () => {
    const [rowdata, setRowdata] = useState(Cardinf);
    console.log(rowdata)
    return (
        <>
            <div className=" body_container">
                <small className="fs-5 fw-bold">ACADEMIC CALENDAR</small>
                <div className="row row-cols-1 row-cols-sm-3 row-cols-md-4 g-3">
                    <Card demodata={rowdata} />
                </div>
                <div className="d-flex align-items-center text-wrap py-1 mt-4">
                    <span className="fw-bold me-1"><small className="fs-5">SESSION RECORDINGS</small></span>
                    <div className=" d-flex ms-auto">
                        <a href="#upcoming" aria-controls="upcoming" role="tab" data-toggle="tab" className="active text-dark me-2"><small>RECENT</small></a>
                        <a href="/acads/student/videosHome?pageNo=1&amp;academicCycle=Jan2023" className="text-dark me-1"><small className="text-nowrap">SEE ALL</small></a>
                        <a type="button" data-bs-toggle="collapse" href="#collapseThree" role="button" aria-expanded="true" aria-controls="collapseThree" id="collapseCard" className="text-muted ">
                            <i className="fa-solid fa-square-minus"></i>
                        </a>
                    </div>
                </div>
                <div className="row row-cols-1 row-cols-sm-3 row-cols-md-4 g-3 ">
                    <Cardsec demoval={rowdata} />
                </div>
                <div className="row row-cols-1 row-cols-sm-2 row-cols-md-2 g-3 mt-4 p-0">
                    <Cardthree />
                    <Cardthree />
                </div>
            </div>
        </>)
}

export default Maincont