import React from 'react'

const Cardthree = () => {
    return (
        <>    
        <div className='col'>
            <div className="card" >
                <ul className="list-group list-group-flush">
                    <li className="list-group-item border-none p-0"><a href="student/viewCourseDetails?programSemSubjectId=2008 " className=" text-dark fw-semibold list-group-item list-group-item-action">
                        1 Decision Science
                        <span title="Live Subject" className="float-end fs-5"><i className="fa fa-eye " aria-hidden="true"></i></span>
                    </a></li>
                    <li className="list-group-item p-0"><a href="student/viewCourseDetails?programSemSubjectId=2008 " className=" text-dark fw-semibold list-group-item list-group-item-action">
                        1 Decision Science
                        <span title="Live Subject" className="float-end fs-5"><i className="fa fa-eye " aria-hidden="true"></i></span>
                    </a></li>
                    <li className="list-group-item p-0">
                    <a href="student/viewCourseDetails?programSemSubjectId=2008 " className=" text-dark fw-semibold list-group-item list-group-item-action">
								1 Decision Science
									 	<span title="Live Subject" className="float-end fs-5"><i className="fa fa-eye " aria-hidden="true"></i></span>	
							</a>
                    </li>
                </ul>
            </div>
            </div>
        </>
    )
}

export default Cardthree