import React from 'react'

const Card = ({ demodata }) => {
    return (
        <>

            {demodata.map((val) => {
                return (
                    <>
                        <div className="col">
                            <div className="card">
                                <div className="card-body">
                                    <div className="text-nowrap d-block text-truncate w-100">
                                        <small className="text-dark"><span className=" sz-date myDate ">02-Mar-2023</span>

                                            <span className=" sz-time myTime">05:30:00 AM</span></small>
                                    </div>
                                    <div className="clearfix"></div>
                                    <div className="sz-calndr-info">
                                        <p className="d-block text-truncate w-100 mb-3 fw-bolder" data-bs-toggle="tooltip" title="Decision Science">
                                            Decision Science
                                        </p>
                                        <p className="d-block text-truncate w-100" data-bs-toggle="tooltip" title="Session 6">Session 6</p>
                                        <p className="WeekDayBatch-text">WeekDay Batch</p>

                                    </div>
                                    <div className="row text-nowrap">
                                        <div className="col-9 text-truncate">
                                            <span className="text-black " data-bs-toggle="tooltip" title="Prof.Rooshabh Mehta">
                                                Prof.Rooshabh Mehta</span>
                                        </div>

                                        <div className="col">

                                            <span title="Live Subject " className="float-end mt-2">
                                                <i className="fa-regular fa-eye text-black"></i>
                                            </span>

                                        </div>
                                    </div>
                                    <h5 className="card-title">{val.id}</h5>

                                </div>
                            </div>
                        </div>
                    </>
                )
            })}

        </>

    )
}

export default Card