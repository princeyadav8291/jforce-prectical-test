import React from 'react'
import Footer from './Footer'
import Maincont from './Maincont'
import Navbar from './Navbar'
import Sidebars from './Sidebars'

const index = () => {
    
    
    return (
        <>

                <Sidebars />         
            <div className='main-content'>
                <div className="header">
                    <Navbar />
                    <div className="p-4 p-md-5 mb-4  text-white bg-dark bg-image" >
                        <div className="col-md-6 px-0">
                            <h3 className="student-name">SAURABH PAWAR</h3>
                            <p className="lead mb-0"><a href="#" className="text-white fw-bold">Continue reading...</a></p>
                        </div>
                    </div>
                </div>
                <Maincont />
            </div>
        </>
    )
}

export default index